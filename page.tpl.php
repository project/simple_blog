<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">

<head>
  <title><?php if ($node->type == 'wiki'){print $title;} else {print $head_title;} ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
<?php if (ie_detect()){ ?>

<!--[if lte IE 6]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6-fix.css";</style>
<![endif]-->
<!--[if lte IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie6-fix.css";</style>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/ie7-fix.css";</style>
<![endif]-->
<?php } ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
	<script>$(document).ready(function () {$(".comment:even").addClass("oddrow"); });</script>
</head>

<body>

<div id="wrapper">
	
	<div id="head">
		
		<div id="logo">
			<?php if ($logo) { ?>
			<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
			<?php } ?>
		</div>
		<?php if($search_box):?>
			<div id="searchBox"><?php print $search_box;?></div>
		<?php endif;?>
		<div id="logolinks"><div id="sidebar-left"><?php print $sidebar_left ?></div></div>
		
		<div id="primary"><div id="primarywrap"><?php print theme('links', $primary_links) ?></div></div>
	</div>

	<?php if ($is_front): ?><div id="headerwrap"><?php print $header ?></div><div id="promobottom">&nbsp;</div><?php endif; ?>

	<div id="container">
		
		<div id="contentwrap">
			<?php print $tabs ?>
			<?php print $messages ?>
			<!-- ?php print $breadcrumb ? -->
			<div class="title"><?php print $title ?></div><br />
			<?php print $content ?>
			<div class="clear">&nbsp;</div>
		</div>
		<div id="sidebar-right"><?php print $right ?></div>
		<div class="clear">&nbsp;</div>
	</div>


	<div id="footer"><div id="footertxt">
	<?php print theme('links', $secondary_links); 
	print $footer; ?></div>
  <div style="width: 100px; float: right; font-size: 80%; text-align: right; padding-right: 10px;">
  <!-- Please Keep this link and Do not remove it, thank you -->
  	Design by <a href="http://ekikrat.in" style="color: gray;">Ekikrat</a>
  	</div>
			
			<br /><br />
			<?php print $footer_message ?>
	</div>
	
</div>



<?php print $closure ?>
</body>
</html>
